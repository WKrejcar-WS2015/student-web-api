﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using SampleWebApiCrud.Models;

namespace SampleWebApiCrud.Controllers
{
    public class StudentController : ApiController
    {
        private readonly IDataLayer _dataLayer;

        public StudentController(IDataLayer dataLayer)
        {
            _dataLayer = dataLayer;
        }

        // GET: api/Student
        public IEnumerable<Student> Get()
        {
            return _dataLayer.ReadAll();
        }

        // GET: api/Student/5
        public Student Get(Guid id)
        {
            throw new NotImplementedException();
        }

        // POST: api/Student
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Student/5
        public void Put(Guid id, [FromBody]Student value)
        {
        }

        // DELETE: api/Student/5
        public void Delete(Guid id)
        {
            _dataLayer.Delete(id);
        }
    }
}
