using Microsoft.Practices.Unity;
using System.Web.Http;
using SampleWebApiCrud.Models;
using Unity.WebApi;

namespace SampleWebApiCrud
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterInstance(typeof(IDataLayer), new DataLayer(), new ContainerControlledLifetimeManager());
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}