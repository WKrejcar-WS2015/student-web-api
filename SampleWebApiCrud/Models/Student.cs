﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleWebApiCrud.Models
{
    public enum Grade
    {
        Freshmen,
        Sophomore,
        Junior,
        Senior
    }

    public class Student
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public Grade Grade { get; private set; }
        public Guid Id { get; private set; }

        public Student(Guid id, string firstName, string lastName, Grade grade)
        {
            if (firstName == null) throw new ArgumentNullException(nameof(firstName));
            if (lastName == null) throw new ArgumentNullException(nameof(lastName));

            FirstName = firstName;
            LastName = lastName;
            Grade = grade;
            Id = id;
        }
    }
}